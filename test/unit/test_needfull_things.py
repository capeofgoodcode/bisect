import unittest

from bisect.needfull_things import AllInOneDeviceSuitableForEveryPurpose

class AllInOneDeviceSuitableForEveryPurposeTest(unittest.TestCase):

    def testDoAction1Heigh(self):
        config = {'usefullness': 'heigh',
                  'frequency': 1}
        device = AllInOneDeviceSuitableForEveryPurpose(config)
        self.assertEqual(device.doAction1(),
                          'doing action1 with heigh usefullness')

    def testDoAction1Low(self):
        config = {'usefullness': 'low',
                  'frequency': 1}
        device = AllInOneDeviceSuitableForEveryPurpose(config)
        self.assertEqual(device.doAction1(),
                          'doing action1 with low usefullness')

    def testDoAction1Frequency(self):
        config = {'usefullness': 'heigh',
                  'frequency': 3}
        device = AllInOneDeviceSuitableForEveryPurpose(config)
        self.assertEqual(
            device.doAction1(),
            '\n'.join(['doing action1 with heigh usefullness'] * 3))
