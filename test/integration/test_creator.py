import unittest

from bisect.creator import create

class CreateTest(unittest.TestCase):

    def testSmoke(self):
        config = {'usefullness': 'heigh'}
        result = create(config)
        self.assertEqual(result,
                         'doing action1 with heigh usefullness\n'
                         'action2 is always very, very useful')
