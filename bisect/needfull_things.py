
class AllInOneDeviceSuitableForEveryPurpose:
    '''
    The one and only class everybody is dreaming of.

    Believe it or not!
    '''

    def __init__(self, config: dict):
        '''
        :param configuration: Dictionary used to configure the class instance.
            Supported keys:
                - 'usefullness': "high" or "low"
                - 'frequency': integer
        '''
        # stores configuration values (redundant comment, but I do not know
        # any better)
        self.config_ = config

    def doAction1(self) -> str:
        return self._applyFrequency('doing action1 with {} usefullness'
                                    .format(self.config_['usefullness']))

    def doAction2(self) -> str:
        return self._applyFrequency('action2 is always very, very useful')

    def doAction3(self) -> str:
        return 'action 3'

    def _applyFrequency(self, action: str) -> str:
        return '\n'.join([action] * self.config_['frequency'])
