import logging
from bisect.needfull_things import AllInOneDeviceSuitableForEveryPurpose

logger = logging.getLogger(__name__)

def create(config: dict) -> str:
    device = AllInOneDeviceSuitableForEveryPurpose(config)
    result1 = device.doAction1()
    result2 = device.doAction2()
    result = result1 + '\n' + result2
    logger.debug(result)
    return result
